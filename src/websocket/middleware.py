from django.urls import resolve
from .connection import WebSocket

def websockets(app):
    async def asgi(scope, receive, send):
        if scope['type'] == 'http':
            # Let Django handle HTTP requests
            await app(scope, receive, send)
        elif scope["type"] == "websocket":
            match = resolve(scope["raw_path"])
            await match.func(WebSocket(scope, receive, send), *match.args, **match.kwargs)
        else:
            raise NotImplementedError(f"Unknown scope type {scope['type']}")
    return asgi