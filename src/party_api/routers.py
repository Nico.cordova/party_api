class PartyMongoRouter:
    """
    A router to control all database operations on models in the
    party application.
    """
    route_app_labels = {'match_manager'}

    def db_for_read(self, model, **hints):
        """
        Attempts to read party models go to mongo_db.
        """
        if model._meta.app_label in self.route_app_labels:
            return 'mongo_db'
        return None

    def db_for_write(self, model, **hints):
        """
        Attempts to write party models go to mongo_db.
        """
        if model._meta.app_label in self.route_app_labels:
            return 'mongo_db'
        return None

    def allow_relation(self, obj1, obj2, **hints):
        """
        Allow relations if a model in the party app is
        involved.
        """
        if (
            obj1._meta.app_label in self.route_app_labels or
            obj2._meta.app_label in self.route_app_labels
        ):
           return True
        return None

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        """
        Make sure the party app only appear in the
        'mongo_db' database.
        """
        if app_label in self.route_app_labels:
            return db == 'mongo_db'
        return None
