from djongo import models
from .utils.fields import EmbeddedDictField

class Room(models.Model):
    name = models.CharField(max_length=100)
    password = models.CharField(max_length=100)
    max_players = models.IntegerField()
    empty = models.BooleanField(default=False)
    private = models.BooleanField(default=True)
    last_updated = models.DateTimeField(auto_now=True)

    objects = models.DjongoManager()

    class Meta:
        app_label = "match_manager"

class Players(models.Model):
    room = models.ForeignKey(Room, on_delete=models.SET_NULL, null=True)
    game_character_id = models.IntegerField(null=True)
    name = models.CharField(max_length=100)
    code = models.IntegerField(null=True)
    position = EmbeddedDictField(model_container=dict)
    passed = models.BooleanField(default=False)
    moved = models.BooleanField(default=False)
    used_skill = models.BooleanField(default=False)

    objects = models.DjongoManager()

    class Meta:
        app_label = "match_manager"
