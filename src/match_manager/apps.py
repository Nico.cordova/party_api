from django.apps import AppConfig


class MatchManagerConfig(AppConfig):
    name = 'match_manager'
