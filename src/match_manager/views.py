from django.views.generic.base import TemplateView
from django.shortcuts import render
def index(request):
    template_name = "index.html"
    return render(request,template_name, {})

async def websocket_view(socket):
    await socket.accept()
    await socket.send_text('hello')
    await socket.close()